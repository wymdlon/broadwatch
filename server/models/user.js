const mongoose = require('mongoose');
const hash = require('../lib/hash');

const UserShema = new mongoose.Schema({
    login: {type: String, required: true},
    password: {type: String, required: true},
    role: {type: Number, default: 0},
    fullname: {type: String, default: 'user'},
    about: {type: String, default: `Hello I'm new here!`},
    created: {type: Date, default: Date.now},
    avaUrl: {type: String, default: 'https://res.cloudinary.com/cloudlab6/image/upload/v1541238486/images/logo/default.png'},
    isDisabled: {type: Boolean, default: false}
});

const UserModel = mongoose.model('User', UserShema);

module.exports = class User {
    constructor(id, login, password) {
        this.id = id;
        this.login = login;
        this.fullname = login;
        this.password = hash.sha512(password).passwordHash;
    }

    static getAll() {
        return UserModel.find();
    }

    static getById(id) {
        return UserModel.findById(id);
    }

    static getByLoginAndHashPass(login, pass){
        return UserModel.findOne({login, password: pass});
    }

    static insert(user) {
        return new UserModel(user).save();
    }

    static update(user){
        return UserModel.findByIdAndUpdate(user.id, 
        {
            fullname: user.fullname,
            about: user.about,
            avaUrl: user.avaUrl
        });
    }

    static async delete(id){
        return UserModel.findByIdAndDelete(id);
    }

    static getByLogin(login){
        return UserModel.findOne({login});
    }

    static changeRole(id, role){
        return UserModel.findByIdAndUpdate(id, { role });
    }
};

