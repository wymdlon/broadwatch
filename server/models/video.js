const mongoose = require('mongoose');
const VideoToPlaylist = require('./videoToPlaylist');

const urlPreviewDefault = 'https://res.cloudinary.com/cloudlab6/image/upload/v1542840434/images/video/default.png';

const VideoShema = new mongoose.Schema({
    name: {type: String, required: true},
    postedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    url: { type: String },
    description: {type: String},
    image: {type: String, default: 'default.png'},
    likes: {type: Number, default: 0},
    dislikes: {type: Number, default: 0},
    created: {type: Date, default: Date.now}
});

const VideoModel = mongoose.model('Video', VideoShema);

class Video {
    constructor(id, postedBy, name, url, image, description = "", likes = 0, dislikes = 0) {
        this.id = id;
        this.postedBy = postedBy;
        this.name = name;
        this.url = url;
        this.description = description;
        this.image = (image) ? image : urlPreviewDefault;
        this.likes = likes;
        this.dislikes = dislikes;
        this.created = new Date().toISOString();
    }

    static getAll(){
        return VideoModel.find().populate('postedBy');
    }

    static getAllUserVideos(idUsers){
        return VideoModel.find({postedBy: idUsers});
    }

    static getCountAllVideos(query){
        if(query)
            return VideoModel.countDocuments({name: new RegExp('^'+query+'$', "i")});
        return VideoModel.countDocuments();
    }

    static getLimitedVideos(limit, page, query){
        if(query)
            return VideoModel.find({name: new RegExp('^'+query+'$', "i")}, {}, { skip: limit*page, limit }).populate('postedBy');
        return VideoModel.find({}, {}, { skip: limit*page, limit }).populate('postedBy');
        
    }

    static getLimitedVideosAPI(limit, page, query){
        if(query)
            return VideoModel.find({name: new RegExp('^'+query+'$', "i")}, {}, { skip: limit*page, limit });
        return VideoModel.find({}, {}, { skip: limit*page, limit });
        
    }

    static insert(video){ 
        return new VideoModel(video).save();
    }
    
    static getById(id){
        return VideoModel.findById(id).populate('postedBy');
    }

    static getByIdAPI(id){
        return VideoModel.findById(id);
    }
    
    static update(video){
        return VideoModel.findByIdAndUpdate(video.id, 
        {
            name: video.name,
            description: video.description,
        });
    }

    static async delete(id){
        try{
            await VideoToPlaylist.deleteVideoFromAllPlaylists(id);
            const res = await VideoModel.findByIdAndDelete(id);
            return Promise.resolve(res);
        }
        catch(err) {
            return Promise.reject(err);
        }
    }

    static deleteAllUserVideo(id){
       return new Promise(async (resolve, reject) => {
        try {
            const videos = await this.getAllUserVideos(id);
            for(let key in videos) {
                const video = videos[key];
                await this.delete(video._id);
            }
            resolve(videos);
        } catch (err) {
            reject(err)
        }
       });
    }
};

module.exports = Video;