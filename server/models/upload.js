const config = require('../config');
const util = require('util');

const cloudinary = require('cloudinary');
// const upload = util.promisify(cloudinary.v2.uploader.upload);

cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

function uploadFile(data, type = 'raw', folder) {
    if(!data) return Promise.resolve(null);

    if(type != 'raw' && type != 'video' && type != 'image') 
        return Promise.reject("Error type");

    let fold = '';
    
    if (type == 'video'){
        fold = 'videos';
    }
    else if (type == 'image'){
        fold = 'images';

        switch(folder) {
            case 'playlist':  
                fold += '/playlist'; break;
            case 'logo': 
                fold += '/logo'; break;      
            case 'video':  
                fold += '/video'; break;
            default: {
                return Promise.reject("Error name folder");
            }
        }
    } 
    else fold = 'raw';

    const options = {
        folder: fold,
        resource_type: type
    }
    
        
    return new Promise((resolve, reject) => {
        cloudinary.v2.uploader.upload_stream(options,
            (error, result) => { 
                if(error) reject(error);
                else resolve(result);
        }).end(data);
    });
}

module.exports = uploadFile;