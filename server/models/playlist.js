const mongoose = require('mongoose');
const VideoToPlaylist = require('./videoToPlaylist');

const defaultImage = 'https://res.cloudinary.com/cloudlab6/image/upload/v1542840434/images/video/default.png';

const PlaylistShema = new mongoose.Schema({
    name: {type: String, required: true},
    postedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    description: {type: String, default: ''},
    image: {type: String, default: defaultImage},
    videos: {
        type: Array
    }
});

const PlaylistModel = mongoose.model('Playlist', PlaylistShema);

class Playlist {
    constructor(id, postedBy, name, description, image, videos = []) {
        this.id = id;
        this.postedBy = postedBy;
        this.name = name;
        this.description = description;
        this.image = (image) ? image : defaultImage;
        this.videos = videos;
    }

    static getAllUserPlaylistsAPI(idUser){
        return new Promise((resolve, reject) => {
            PlaylistModel.find({postedBy: idUser})
                .then(async (playlists) => {
                    for(let i = 0; i < playlists.length; i++){
                        const videosAndPlaylist = await VideoToPlaylist.getAllVideosByPlaylistId(playlists[i]._id);
                        playlists[i].videos = [];
                        for(let item of videosAndPlaylist) {
                            playlists[i].videos.push(item.video);
                        }
                    }
                    resolve(playlists)
                })
                .catch(err => reject(err))
        })
    }

    static insert(playlist){ 
        const videos = playlist.videos;
        playlist.videos = undefined;
        return new Promise(async (resolve, reject) => {
            try{
                const list = [];
                const p = await new PlaylistModel(playlist).save()
                for(let i = 0; i < videos.length; i++) {
                    const vtop = new VideoToPlaylist(p._id, videos[i]);
                    const v = await VideoToPlaylist.insert(vtop);
                    list.push(v.video);
                }
                p.videos = list;
                resolve(p)
            }
            catch (err) { reject(err) }
        })
    }
    
    static getById(id){
        return new Promise((resolve, reject) => {
            Promise.all([VideoToPlaylist.getAllVideosByPlaylistId(id), PlaylistModel.findById(id).populate('postedBy')])
                .then(([vtop, playlist]) => {
                    for(let key in vtop) {
                        playlist.videos.push(vtop[key].video);
                    }
                    resolve(playlist)
                })
                .catch(err => reject(err))
        })
    }
    
    static update(playlist){
        return PlaylistModel.findByIdAndUpdate(playlist.id, 
            {
                name: playlist.name,
                description: playlist.description,
                image: playlist.image,
            });
    }

    static delete(id){
        return new Promise((resolve, reject) => {
            Promise.all([VideoToPlaylist.deleteAllVideosByPlaylistId(id), PlaylistModel.findByIdAndDelete(id)])
                .then(([deletedVtop, playlist]) => {
                    playlist.videos = undefined
                    resolve(playlist)
                })
                .catch(err => reject(err))
        })
    }

    static addVideo(playlistId, videoId){
        const vtop = new VideoToPlaylist(playlistId, videoId);
        return VideoToPlaylist.insert(vtop);
    }
};

module.exports = Playlist;