const mongoose = require('mongoose');

const TelegramUserShema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  username: {
    type: String,
    required: true,
  },
  userTgId: {
    type: String
  },
  chatId: {
    type: String
  }
});

const TelegramUserModel = mongoose.model('TelegramUser', TelegramUserShema);

class TelegramUser {
  constructor(userId, username, userTgId = '', chatId = '') {
    this.userId = userId,
    this.username = username,
    this.userTgId = userTgId,
    this.chatId = chatId
  }

  static async isExist(username) {
    try{
      const user = await TelegramUserModel.findOne({username});
      return user ? true : false;
    } catch(err) {
      console.log(err);
      return false;
    }
  }

  static async insert(telegramUser) {
    if(!(await TelegramUser.isExist(telegramUser.username))){
      return new TelegramUserModel(telegramUser).save();
    }
  }

  static updateUsername(telegramUser) {
    return TelegramUserModel.findOneAndUpdate({userId: telegramUser.userId}, 
      {username: telegramUser.username}
    );
  }

  static update(telegramUser) {
    return TelegramUserModel.findOneAndUpdate({userId: telegramUser.userId}, 
      {chatId: telegramUser.chatId, username: telegramUser.username}
    );
  }

  static updateByUsername(telegramUser) {
    return TelegramUserModel.findOneAndUpdate({username: telegramUser.username}, 
      {chatId: telegramUser.chatId, userTgId: telegramUser.userTgId}
    );
  }

  static delete(userId) {
    return TelegramUserModel.findOneAndDelete({userId})
  }
  
  static getUser(userId) {
    return TelegramUserModel.findOne({userId});
  }

  static getUserByUsername(username) {
    return TelegramUserModel.findOne({username});
  }
}

module.exports = TelegramUser;