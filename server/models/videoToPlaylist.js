const mongoose = require('mongoose');

const VideoToPlaylistShema = new mongoose.Schema({
  video: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Video',
    required: true,
    formType: String
  },
  playlist: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Playlist',
    required: true,
    formType: String
  }
});

const VideoToPlaylistModel = mongoose.model('VideoToPlaylist', VideoToPlaylistShema);

class VideoToPlaylist {
  constructor(playlistId, videoId){
    this.playlist = playlistId;
    this.video = videoId;
  }

  static getAllVideosByPlaylistId(playlistId) {
    return VideoToPlaylistModel.find({playlist: playlistId}).populate('video')
  }

  static getAllPlaylistsByVideoId(videoId) {
    return VideoToPlaylistModel.find({video: videoId})
  }

  static insert(videoToPlaylist) {
    return new VideoToPlaylistModel(videoToPlaylist).save();
  }

  static deleteAllVideosByPlaylistId(playlistId) {
    return VideoToPlaylistModel.deleteMany({playlist: playlistId});
  }

  static deleteVideoFromAllPlaylists(videoId) {
    return VideoToPlaylistModel.deleteMany({video: videoId});
  }

  static deleteVideoFromPlaylist(playlistId, videoId) {
    return VideoToPlaylistModel.deleteOne({playlist: playlistId, video: videoId})
  }

}

module.exports = VideoToPlaylist;
