const mongoose = require('mongoose');

const SubscriberShema = new mongoose.Schema({
  channelId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  subscriberId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Telegram',
    required: true,
  }
});

const SubscriberModel = mongoose.model('Subscriber', SubscriberShema);

class Subscriber {
  constructor(channelId, subscriberId){
    this.channelId = channelId;
    this.subscriberId = subscriberId;
  }

  static subscribe(subscriber){
    return new SubscriberModel(subscriber).save()
  }

  static unsubscribe(subscriber){
    return SubscriberModel.deleteMany({channelId: subscriber.channelId, subscriberId: subscriber.subscriberId});
  }

  static async isSubscribe(channelId, userId){
    return (await SubscriberModel.findOne({channelId, subscriberId: userId})) ? true : false;
  }

  static async getAllSubscribers(channelId) {
    const subscribers = [];
    const res = await SubscriberModel.find({channelId})
    for(let key in res) {
      subscribers.push(res[key].subscriberId);
    }
    return subscribers;
  }
}

module.exports = Subscriber;