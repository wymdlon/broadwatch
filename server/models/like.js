const mongoose = require('mongoose');

const LikeShema = new mongoose.Schema({
  videoId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Video',
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  value: {
    type: String
  }
});

const LikeModel = mongoose.model('Like', LikeShema);

class Like {
  constructor(videoId, userId, value){
    this.videoId = videoId;
    this.userId = userId;
    this.value = value;
  }

  static rate(like) {
    return new LikeModel(like).save();
  }

  static async isRated(like) {
    return (await this.getValue(like)) ? true : false;
  }

  static getValue(like){
    return LikeModel.findOne({
      videoId:  mongoose.Types.ObjectId(like.videoId),
      userId:  mongoose.Types.ObjectId(like.userId)
    })
  }

  static updated(like) {
    return LikeModel.findOneAndUpdate(
    {
      videoId: mongoose.Types.ObjectId(like.videoId),
      userId: mongoose.Types.ObjectId(like.userId)
    },
    {
      value: like.value
    })
  }

  static delete(like) {
    return LikeModel.findOneAndDelete({
      videoId: mongoose.Types.ObjectId(like.videoId),
      userId: mongoose.Types.ObjectId(like.userId)
    })
  }

  static countLikes(videoId) {
    return LikeModel.countDocuments({videoId, value: 'like'})
  }

  static countDislikes(videoId) {
    return LikeModel.countDocuments({videoId, value: 'dislike'})
  }
}

module.exports = Like;