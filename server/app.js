const path = require('path');

const config = require('./config');
require('./lib/telegram');

// express
const express = require('express');

// all parsers
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');

const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');
// dbs
const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);

const connectOptions = {useNewUrlParser: true};

const app = express();

const cors = require('cors');
app.use(cors());

// settings start
app.use(express.static(path.join(__dirname, 'dist')));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
 
// parse application/json
app.use(bodyParser.json())

// parse files
app.use(busboyBodyParser({limit: '100mb'}));


// // passport
// app.use(passport.initialize());
// app.use(passport.session());
require('./config/passport');


// setting end

const routerAPI = require('./routers/api');
app.use('/api/v1', routerAPI);


app.use('*', function (req, res) {
    res.sendFile(process.cwd() + "/dist/index.html");
})

const dbUrl = config.database.url;
const PORT = config.server.port;

mongoose.connect(dbUrl, connectOptions)
    .then(() => console.log(`Database connected: ${dbUrl}`))
    .then(() => app.listen(PORT, () => { console.log(`Server started on http://localhost:${PORT}`); }))
    .catch(err => { console.log(`Start error ${err}`); });

