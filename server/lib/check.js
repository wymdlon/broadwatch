const mongoose = require('mongoose');
const path = require('path');

function auth(req, res, next) {
    if (!req.user) return res.sendStatus(401); // 'Not authorized'
    next();  
}

function admin(req, res, next) {
    if (!req.user) res.sendStatus(401); // 'Not authorized'
    else if (req.user.role !== 1) res.sendStatus(403); // 'Forbidden'
    else next();
}

function isValidId(id){
    return mongoose.Types.ObjectId.isValid(id);
}

function isValidRate(string) {
    return string == 'like' || string == 'dislike' || string == 'none'
}

function isImage(filename) {
    var ext = path.extname(filename);
    switch (ext.toLowerCase()) {
    case '.jpg':
    case '.jpeg':
    case '.gif':
    case '.bmp':
    case '.png':
        //etc
        return true;
    }
    return false;
}

exports.auth = auth;
exports.admin = admin;
exports.isValidId = isValidId;
exports.isImage = isImage;
exports.isValidRate = isValidRate;