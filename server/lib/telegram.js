process.env["NTBA_FIX_319"] = 1;
const TelegramBot = require('node-telegram-bot-api');
const TelegramUser = require('../models/telegram');
const User = require('../models/user');
const Video = require('../models/video');
const Subscriber = require('../models/subscriber');
const config = require('../config/index');

const token = config.telegram.token;

const api = new TelegramBot(token, {polling: true});

const baseURL = 'https://broadwatch.herokuapp.com';

api.on('message', async (message) =>
{
  try{
    const telegramUser = await TelegramUser.getUserByUsername(message.from.username);
    if(telegramUser){
      if(!telegramUser.userId) {
        sendNotification(message.chat.id, "You succes add to out users! Please send /start for correct work");
      }
      if(message.text === '/start'){
        const tgUser = new TelegramUser(null, message.from.username, message.from.id, message.chat.id);
        await TelegramUser.updateByUsername(tgUser);
        sendNotification(message.chat.id, "You success register");
      } else if (message.text === '/mysubs') {
        const subscribers = await getSubscribers(message.from.username);
        if(subscribers.length == 0)
          sendNotification(message.chat.id, "You have not subscribers!");
        else {
          let response = `You have ${subscribers.length} subscribers:\n`;
          for(let index in subscribers) {
            response += `${(parseInt(index) + 1)}) ${subscribers[index]}\n`;
          }
          sendNotification(message.chat.id, response);
        }
         
      } else if (message.text === '/myvideos'){
        const videos = await getVideos(message.from.username)
        if(videos.length == 0)
          sendNotification(message.chat.id, "You have not videos!");
        else {
          let response = `You have ${videos.length} videos:\n`;
          for(let index in videos) {
            const video = videos[index];
            response += `${(parseInt(index) + 1)}) <a href="${video.url}">${video.name}</a>\n`;
          }
          sendNotification(message.chat.id, response, {parse_mode : "HTML"});
        }
      } else sendNotification(message.chat.id, "Incorrect message.")

    } else {
      sendNotification(message.chat.id, "You don't register.");
    }
  }
  catch (err) {
    sendNotification(message.chat.id, `Something went wrong. ${err.toString()}`);
  }
});

function sendNotification(chatId, message, options) {
  if(!chatId) return;
  api.sendMessage(chatId, message, options)
}

async function getVideos(username) {
  try {
    const tgUser = await TelegramUser.getUserByUsername(username);
    if (!tgUser || !tgUser.userId) return [];
    const videos = await Video.getAllUserVideos(tgUser.userId)
    if (!videos) return [];
    console.log(videos);
    const listVideo = [];
    for(let key in videos) {
      listVideo.push({
        name: videos[key].name, 
        url: `${baseURL}/videos/${videos[key]._id}`
      })
    }
    return listVideo;
  } catch (err) {
    return err;
  }
}

async function getSubscribers(username) {
  try {
    const tgUser = await TelegramUser.getUserByUsername(username);
    if (!tgUser || !tgUser.userId) return [];
    const subs = await Subscriber.getAllSubscribers(tgUser.userId);
    if (!subs) return [];
    const listSubs = [];
    for(let key in subs) {
      const user = await User.getById(subs[key]);
      listSubs.push(user.fullname);
    }
    return listSubs;
  } catch (err) {
    return err;
  }
}

exports.sendNotification = sendNotification;
