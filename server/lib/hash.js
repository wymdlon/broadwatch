const crypto = require('crypto');
const config = require('../config');

const salt = config.server.salt;

function sha512(password){
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt,
        passwordHash: value
    };
};

exports.sha512 = sha512;