const router = require('express').Router();
const passport = require('passport');
const TelegramUser = require('../models/telegram');


const routerAuth = require('./api/auth');
router.use('/auth', routerAuth);

router.use(passport.authenticate('jwt', {session: false}));

router.get('/me', async (req, res) => {
    const user = req.user;
    const tgUser = await TelegramUser.getUser(user._id);
    const tgUsername = (tgUser && tgUser.username) ? tgUser.username : '';
    res.json({
        role: user.role,
        fullname: user.fullname,
        about: user.about,
        avaUrl: user.avaUrl,
        _id: user._id,
        login: user.login,
        created: user.created,
        telegramUsername: tgUsername
    });
});

const routerUser = require('./api/users');
router.use('/users', routerUser);

const routerVideos = require('./api/videos');
router.use('/videos', routerVideos);

router.use('*', (req, res) => {
    res.status(400).json({error: "Handler not found"});
});

module.exports = router;