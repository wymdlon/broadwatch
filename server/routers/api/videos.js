const router = require('express').Router();
const Video = require('../../models/video');
const Subscriber = require('../../models/subscriber');
const TelegramUser = require('../../models/telegram');
const Like = require('../../models/like');
const bot = require('../../lib/telegram');
const upload = require('../../models/upload');
const check = require('../../lib/check');
const path = require('path');

router.get('/', 
    check.auth,
    async (req, res) => {
    const strQuery = req.query.q;
    
    try{
        if(req.query.p && isNaN(req.query.p))
            return res.status(400).json({error: "Bad 'p' query"});

        const limit = 5;
        const numPage = (parseInt(req.query.p) || 1) - 1;
        const allCountDocuments =  await Video.getCountAllVideos(strQuery);
        const countPage = Math.ceil(allCountDocuments / limit);
        
        if(allCountDocuments == 0)
            return res.status(404).json({error: "Page not found"});
        if(req.query.p && (req.query.p < 1 || req.query.p > countPage))
            return res.status(404).json({error: "Page not found"});
        
        const list = await Video.getLimitedVideos(5, numPage, strQuery);
        
        let listPages = [];
        const lim = (numPage + 2 >= countPage) ? countPage : numPage+2;
        for(let i = numPage||1; i <= lim; i++)
            listPages.push(i);

        res.json({countPage,currentPage: numPage + 1, query: strQuery, videos: list });
    } 
    catch(err) {
        res.status(500).json({error: err.message});
        console.log(err);
    };
});

router.get('/my', 
check.auth,
async (req, res) => {
    Video.getAllUserVideos(req.user.id)
        .then(allVideos => {
            res.json(allVideos);
        })
        .catch(err => {
            res.status(500).send(err);
        })
});

router.post('/new', 
    check.auth,
    (req, res) => {
    const uploadVideo = req.files.video;
    
    const preview = (req.files.preview && isImage(req.files.preview.name)) ? req.files.preview  : {};
    
    if(!isCorrectInput(preview, uploadVideo, req.body.name) || !uploadVideo || !isVideo(uploadVideo.name))
        return res.status(400).json({error: "Invalid input"});
    
    const name = req.body.name.trim();
        
    Promise.all([upload(uploadVideo.data, 'video'), upload(preview.data, 'image', 'video')])
        .then(([resp1, resp2]) => {
            const urlVideo = resp1.url;
            const urlPreview = (resp2) ? resp2.url: null;
            
            const userId = req.user.id;
            const video = new Video(null, userId, name, urlVideo, urlPreview, req.body.description);
            return Video.insert(video);
        })
        .then(async video => {
           try {
                const subscribers = await Subscriber.getAllSubscribers(req.user._id);
                for(let key in subscribers){
                    const user = await TelegramUser.getUser(subscribers[key]);
                    if (user && user.chatId.toString().trim())
                        await bot.sendNotification(user.chatId, `Channel ${req.user.fullname} posted new video: <a href="https://broadwatch.herokuapp.com/videos/${video._id}">${video.name}</a>`,{parse_mode : "HTML"});
                }

                res.status(201).json(video);
           } catch(err) {
               console.log(err);
           }
        })
        .catch(err => { res.status(500).json({error: `Can't save preview`}); console.log(err); });        
});

router.get('/:id', 
    check.auth,
    async (req, res) => {
    
    try{
        const id = req.params.id;
        if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});
        
        const video = await Video.getById(id);
        if(!video) return res.status(404).json({error: "Video not found"});

        const likes = await Like.countLikes(video._id);
        const dislikes = await Like.countDislikes(video._id);
        const like = new Like(video._id, req.user._id);
        const value = await Like.getValue(like);
        const likeValue = (value) ? value.value : 'none';
        
        res.json({
            image: video.image,
            likes: likes,
            dislikes: dislikes,
            likeValue: likeValue,
            _id: video._id,
            postedBy: video.postedBy,
            name: video.name,
            url: video.url,
            description: video.description,
            created: video.created
        });
    }
    catch(err){ 
        res.status(500).json({error: err.message}); console.log(err); 
    }
});

router.delete('/:id', 
    check.auth,
    async (req, res) => { 
        try{
            const id = req.params.id;  
            if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});
            
            const vid = await Video.getById(id);
            if(!vid) return res.status(404).json({error: "Video not found"});
                
            if(vid.postedBy.id != req.user.id && req.user.role == 0)
                return res.status(403).json({error: "No access"});

            const video = await Video.delete(id);
            res.json(video);
        }    
        catch(err) { 
            res.status(500).json({error: err.message}); 
            console.log(err); 
        }    
    }
);

router.put('/:id', 
    check.auth,
    async (req, res) => {
    try{
        const id = req.params.id;
        if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});

        const video = await Video.getById(id);
        if(!video) return res.status(404).json({error: "Video not found"});
        
        if(video.postedBy.id != req.user.id && req.user.role == 0)
            return res.status(403).json({error: "No access"})
    
        video.name = (req.body.name) ? req.body.name : video.name;
        video.description = (req.body.description) ? req.body.description : video.description;
    
        const v = await Video.update(video);
        res.json({video: v});
    }
    catch(err){ 
        res.status(500).json({error: "Can't update video"}); console.log(err); 
    }    
});

router.patch('/:id', 
    check.auth,
    async (req, res) => {
    try{
        const id = req.params.id;
        if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});

        const video = await Video.getById(id);
        if(!video) return res.status(404).json({error: "Video not found"});
        
        const rate = req.body.rate;
        if(!rate || !check.isValidRate(rate)) return res.status(400).json({error: "Rate value is not correct"});
    
        const like = new Like(video._id, req.user._id, rate);

        if(rate === 'none') {
            await Like.delete(like)
            return res.json({success: 'OK'});
        }

        if(await Like.isRated(like))
            await Like.updated(like);
        else await Like.rate(like);

        return res.json({success: 'OK'});
    }
    catch(err){ 
        res.status(500).json({error: "Can't rate the video"}); console.log(err); 
    }    
});

function isCorrectInput(preview, uploadVideo, name){
    if(!uploadVideo || uploadVideo.truncated || !name)
        return false;
    
    if(preview && preview.truncated){
        res.status(400).end();
        return false;
    }
    
    return true;
}

function isImage(filename) {
    var ext = path.extname(filename);
    switch (ext.toLowerCase()) {
    case '.jpg':
    case '.gif':
    case '.bmp':
    case '.png':
        //etc
        return true;
    }
    return false;
}

function isVideo(filename) {
    var ext = path.extname(filename)
    switch (ext.toLowerCase()) {
    case '.m4v':
    case '.avi':
    case '.mpg':
    case '.mp4':
        // etc
        return true;
    }
    return false;
}

module.exports = router;

