const router = require('express').Router();
const Video = require('../../models/video');
const Playlist = require('../../models/playlist');
const upload = require('../../models/upload');
const check = require('../../lib/check');

const getUserId = (req, res, next) => {
    req.params.userId = req.originalUrl.split('/')[4];
    if(!check.isValidId(req.params.userId)) 
        return res.status(400).json({error: "User ID is not correct"});
    
    next();
};

router.post('/new',
    check.auth,
    getUserId,
    parseBodyArray('selected'),
    (req, res) => {

        const userId = req.params.userId;
        if(userId != req.user.id && req.user.role == 0)
            return res.status(403).json({error: "No access"});

        const preview = (req.files.preview) ? req.files.preview : {};

        if(!req.body.name || !req.body.selected)
            return res.status(400).json({error: "Invalid input"});

        const sel = req.body.selected;
        upload(preview.data, 'image', 'playlist')
            .then(response => {
                const url = (response) ? response.url : null;
                
                const playlist = new Playlist(null, userId, req.body.name, req.body.description, url, sel);
                return Playlist.insert(playlist);
            })
            .then(playlist => {
                res.json(playlist);
            })
            .catch(err => {
                res.status(500).json({error: err.message}); console.log(err); 
            }
        ); 
    }
);

router.get('/', 
    check.auth,
    getUserId,
    async (req, res) => {
        try{
            const userId = req.params.userId;
            const playlists = await Playlist.getAllUserPlaylistsAPI(userId);
            
            res.json(playlists);
        }
        catch(err){ 
            res.status(500).json({error: err.message}); console.log(err); 
        };
    }
);

router.get('/:id', 
    check.auth,
    getUserId,
    async (req, res) => {       
        try{
            const id = req.params.id;
            if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});

            const playlist = await Playlist.getById(id);
            if(!playlist) return res.status(404).json({error: "Playlist not found"});
            if(playlist.videos.length == 0) return res.status(404).json({error: "Playlist is empty"});
            
            const videos = playlist.videos;
            const currentVideo = await Video.getById(req.query.video) || await Video.getById(playlist.videos[0].id);

            // currentVideo.postedBy = currentVideo.postedBy.id;

            const index = videos.findIndex((value) => {
                if(value.id == currentVideo.id)
                    return true;
            })
            videos.splice(index, 1);

            res.json({currentVideo, otherVideo: videos});
        }
        catch(err){ 
            res.status(500).json({error: err.message}); console.log(err); 
        }    
    }
);

router.delete('/:id', 
    check.auth,
    getUserId,
    async (req, res) => {
    try{
        const id = req.params.id;
        if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});
        
        const playlist = await Playlist.getById(id);
        if(!playlist) return res.status(404).json({error: "Playlist not found"});

        if(playlist.postedBy.id != req.user.id && req.user.role == 0)
            return res.status(403).json({error: "No access"});
        
        const p = await Playlist.delete(id);
        res.json(p);  
    }
    catch(err) {
        res.status(500).json({error: err.message}); console.log(err); 
    }

});

router.put('/:id', 
    check.auth,
    async (req, res) => {
    try{
        const id = req.params.id;
        if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});
        
        const playlist = await Playlist.getById(id);
        if(!playlist) return res.status(404).json({error: "Playlist not found"});

        if(playlist.postedBy.id != req.user.id && req.user.role == 0)
            return res.status(403).json({error: "No access"});

        const preview = (req.files.preview && check.isImage(req.files.preview.name)) ? req.files.preview  : {};
        const resp = await upload(preview.data, 'image', 'playlist');
    
        playlist.name = (req.body.name) ? req.body.name : playlist.name;
        playlist.description = (req.body.description) ? req.body.description : playlist.description;
        playlist.image = (resp && resp.url) ? resp.url : playlist.image;
    
        const p = await Playlist.update(playlist);
        res.json(p);
    }
    catch(err){ 
        res.status(500).json({error: "Can't update playlist"}); console.log(err); };    
    }
);

function parseBodyArray(arrayName) {
    return (req, res, next) => {
        const items = [];
        const obj = req.body;

        for(let key in obj){
            if(key.indexOf(arrayName) != -1)
                items.push(obj[key]);
        }
        req.body[arrayName] = items;
        next();
    };
}


module.exports = router;

