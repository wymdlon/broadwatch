const router = require('express').Router();
const User = require('../../models/user');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const config = require('../../config');


router.post('/register', async (req, res) => {
    if(!req.body.username || !req.body.pass1 || !req.body.pass2)
        return res.status(400).json({error: "Bad user request"});

    let username = req.body.username;
    let pass1 = req.body.pass1;
    let pass2 = req.body.pass2;

    const u = await User.getByLogin(username);
    if(u) return res.status(400).json({error: "User with this login already exist"});

    if(pass1 === pass2){        
        try{
            const user = await User.insert(new User(null, username, pass1)); 
            
            if(!user) return res.status(400).json({error: "User already exist"});
            user.password = undefined;
            
            res.status(201).json(user);
        }
        catch(err){
            res.status(500).json({error: err});
            console.log(err);
        }
    }
    else res.status(400).json({error: "Password not match"});
});

router.post("/login", function (req, res) {
    passport.authenticate('local', { session: false }, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: `Something is not right: ${JSON.stringify(info)}`,
                user: user ? user : null
            });
        }
        req.login(user, { session: false }, (err) => {
            if (err) { return res.send(err); }
            // generate a signed json web token with the contents of user object
            const payload = {id: user._id, username: user.username};
            const token = jwt.sign(payload, config.server.secret);
            return res.json({ token });
        });
    })(req, res);
 });
 

// router.get('/logout', (req, res) => {
//     req.logout();
// });

module.exports = router;