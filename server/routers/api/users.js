const router = require('express').Router();
const User = require('../../models/user');
const Video = require('../../models/video');
const TelegramUser = require('../../models/telegram');
const Subscriber = require('../../models/subscriber')
const check = require('../../lib/check');
const upload = require('../../models/upload');

router.get('/', 
    check.admin,
    async (req, res) => {
        try{
            const users = await User.getAll();
            res.json({users});
        }
        catch(err) { 
            res.status(500).json({error: err}); 
            console.log(err);
        };
    }
);

router.get('/:id', 
    check.auth,
    async (req, res) => {
        try{
            const id = req.params.id;
            if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});

            const user = await User.getById(id);
            if(!user) return res.status(404).json({error: "User not found"});
            user.password = undefined;
            const subscribe = await Subscriber.isSubscribe(id, req.user._id);

            const tgUser = await TelegramUser.getUser(user._id);
            const tgUsername = (tgUser && tgUser.username) ? tgUser.username : '';
            
            res.json({
                role: user.role,
                fullname: user.fullname,
                about: user.about,
                avaUrl: user.avaUrl,
                _id: user._id,
                login: user.login,
                created: user.created,
                telegramUsername: tgUsername,
                subscribe: subscribe
            });            
        }
        catch(err) { 
            console.log(err);
            res.status(500).json({error: err.message});
        };
    }
);

router.put('/changerole', 
    check.admin,
    async (req, res) => {
        try{
            const role = (req.body.role == "admin") ? 1 : (req.body.role == "user") ? 0 : -1;
            if(role == -1) return res.status(400).json({error: "Bad role"});
            
            const id = req.body.id;
            if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});

            const u = await User.getById(id);
            if(!u) return res.status(404).json({error: "User not found"});
            
            const user = await User.changeRole(id, role);
            if(!user) return res.status(500).json({error: "Can't change role"});
            user.password = undefined;

            res.json(user);           
        }
        catch(err) { 
            console.log(err);
            res.status(500).json({error: err.message});
        };
    }
);

router.put('/:id/subscriber', 
    check.auth,
    async (req, res) => {
        try{
            const role = (req.body.subscribe == "subscribe") ? 1 : (req.body.subscribe == "unsubscribe") ? 0 : -1;
            if(role == -1) return res.status(400).json({error: "Bad subscribe field"});

            const id = req.params.id;
            if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});

            const u = await User.getById(id);
            if(!u) return res.status(404).json({error: "User not found"});
            
            const idSub = req.user.id; 
            const sub = new Subscriber(id, idSub);
            
            const result = (role) ? await Subscriber.subscribe(sub) : await Subscriber.unsubscribe(sub);
        
            res.json(result);           
        }
        catch(err) { 
            console.log(err);
            res.status(500).json({error: err.message});
        };
    }
);

router.put('/:id', 
    check.auth,
    async (req, res) => {
        try{
            const id = req.params.id;
            if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});

            const u = await User.getById(id);
            if(!u) return res.status(404).json({error: "User not found"});

            const avatar = (req.files.avatar && check.isImage(req.files.avatar.name)) ? req.files.avatar  : {};
            const resp = await upload(avatar.data, 'image', 'logo');
            
            u.about = (req.body.about) ? req.body.about : u.about;
            u.fullname = (req.body.fullname) ? req.body.fullname : u.fullname;
            u.avaUrl = (resp && resp.url) ?  resp.url : u.avaUrl;

            const user = await User.update(u);
            if(!user) return res.status(500).json({error: "Can't update profile"});

            user.password = undefined;

            res.json(user);           
        }
        catch(err) { 
            console.log(err);
            res.status(500).json({error: err.message});
        };
    }
);

router.patch('/:id', 
    check.auth,
    async (req, res) => {
        try{
            const id = req.params.id;
            const username = req.body.username;
            if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});
            if(username.length <= 0){
                const tgU = await TelegramUser.getUser(id);
                if(tgU) {
                    tgU.username = username
                    await TelegramUser.delete(id);
                }
                return res.json({ok: "success"});
            } 

            const currUs = await User.getById(id);
            if(!currUs) return res.status(404).json({error: "User not found"});

            const tu = await TelegramUser.getUserByUsername(username);
            if(tu && tu.userId.toString() != currUs._id.toString()) return res.status(400).json({error: "Username already used"});
            if(tu && tu.username == username) return res.json({ok: "success"});


            const tgUser = new TelegramUser(currUs.id, username);
            const existUser = await TelegramUser.getUser(currUs.id);
            
            const tUser = (!existUser) ? await TelegramUser.insert(tgUser) : await TelegramUser.updateUsername(tgUser);

            res.json(tUser);           
        }
        catch(err) { 
            console.log(err);
            res.status(500).json({error: err.message});
        };
    }
);

router.delete('/:id', 
    check.auth,
    async (req, res) => { 
        try{
            const id = req.params.id;  
            if(!check.isValidId(id)) return res.status(400).json({error: "Id is not correct"});
            
            const u = await User.getById(id);
            if(!u) return res.status(404).json({error: "User not found"});
                
            if(u.id != req.user.id && req.user.role == 0)
                return res.status(403).json({error: "No access"});

            const user = await User.delete(id);
            await Video.deleteAllUserVideo(id);
            user.password = undefined;
            res.json(user);
        }    
        catch(err) { 
            res.status(500).json({error: err.message}); 
            console.log(err); 
        }    
    }
);

router.use('/:id/playlists', require('./playlists'));

module.exports = router;