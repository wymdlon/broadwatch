import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faVideo, faUsers, faUser, faAddressCard, faUpload, faHome, faIdCard, faSignOutAlt, faThumbsUp, faThumbsDown } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faVideo, faUsers, faUser, faAddressCard, faUpload, faHome, faIdCard, faSignOutAlt, faThumbsUp, faThumbsDown)

Vue.component('font-awesome-icon', FontAwesomeIcon)

// config.autoAddCss = false
