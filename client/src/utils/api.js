import axios from 'axios'

axios.defaults.headers.common['Authorization'] = ''
const base = '/api/v1'
const setToken = function (token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
}
const login = function (body) {
  return axios.post(base + '/auth/login', body)
}

const register = function (body) {
  return new Promise((resolve, reject) => {
    axios.post(base + '/auth/register', body)
      .then(response => {
        resolve(response.data)
      })
      .catch(err => {
        reject(err.response.data.error)
      })
  })
}

const getEntity = function (url) {
  return axios.get(base + url)
}

const updateEntity = function (url, body) {
  return axios.put(base + url, body)
}

const loadEntity = function (url, body) {
  return axios.post(base + url, body)
}

const deleteEntity = function (url) {
  return axios.delete(base + url)
}

const patchEntity = function (url, body) {
  return axios.patch(base + url, body)
}

export { setToken, login, getEntity, register, updateEntity, loadEntity, deleteEntity, patchEntity }
