import { USER_REQUEST, USER_ERROR, USER_SUCCESS } from '../actions/user'
import { getEntity, setToken } from '../../utils/api'
import Vue from 'vue'
import { AUTH_LOGOUT } from '../actions/auth'

const state = { status: '', profile: null }

const getters = {
  getProfile: state => state.profile,
  isProfileLoaded: state => !!(state.profile && state.profile._id),
  isAdmin: state => (state.profile && state.profile.role === 1)
}

const actions = {
  [USER_REQUEST]: ({ commit, dispatch }) => {
    setToken(localStorage.getItem('user-token'))
    commit(USER_REQUEST)
    getEntity('/me')
      .then(resp => {
        commit(USER_SUCCESS, resp.data)
      })
      .catch(resp => {
        commit(USER_ERROR)
        // if resp is unauthorized, logout, to
        dispatch(AUTH_LOGOUT)
      })
  }
}

const mutations = {
  [USER_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [USER_SUCCESS]: (state, resp) => {
    state.status = 'success'
    Vue.set(state, 'profile', resp)
  },
  [USER_ERROR]: (state) => {
    state.status = 'error'
  },
  [AUTH_LOGOUT]: (state) => {
    state.profile = null
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
