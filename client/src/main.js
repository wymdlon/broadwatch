import Vue from 'vue'
import App from './App.vue'
import router from './router'
import '@/plugins/bootstrap'
import '@/plugins/font-awesome'
import store from './store/index'
import Notifications from 'vue-notification'
import VeeValidate from 'vee-validate'
import formLoading from 'vue2-form-loading'

Vue.use(formLoading)
Vue.use(Notifications)
Vue.use(VeeValidate)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
