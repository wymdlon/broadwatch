import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'

import Videos from './views/Videos.vue'
import NewVideo from './views/NewVideo.vue'
import Video from './views/Video.vue'

import Users from './views/Users.vue'
import User from './views/User.vue'

import NewPlaylist from './views/NewPlaylist.vue'
import Playlists from './views/Playlists.vue'
import Playlist from './views/Playlist.vue'

import store from './store/index'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
}
const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
}

const ifAdmin = (to, from, next) => {
  if (store.getters.isAdmin) {
    next()
    return
  }
  next('/')
}

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      beforeEnter (to, from, next) {
        document.title = to.meta.title
        next()
      },
      name: 'home',
      component: Home,
      meta: { title: 'Broad Watch' }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: { title: 'Login' },
      beforeEnter (to, from, next) {
        ifNotAuthenticated(to, from, next)
        document.title = to.meta.title
        next()
      }
    },
    {
      path: '/register',
      meta: { title: 'Register' },
      beforeEnter (to, from, next) {
        document.title = to.meta.title
        next()
      },
      name: 'register',
      component: Register
    },
    {
      path: '/videos',
      meta: { title: 'Videos' },
      beforeEnter (to, from, next) {
        ifAuthenticated(to, from, next)
        document.title = to.meta.title
        next()
      },
      name: 'videos',
      component: Videos
    },
    {
      path: '/videos/new',
      meta: { title: 'Upload new Video' },
      beforeEnter (to, from, next) {
        ifAuthenticated(to, from, next)
        document.title = to.meta.title
        next()
      },
      name: 'videosnew',
      component: NewVideo
    },
    {
      path: '/videos/:id',
      meta: { title: 'Video' },
      beforeEnter (to, from, next) {
        ifAuthenticated(to, from, next)
        document.title = to.meta.title
        next()
      },
      name: 'currentvideo',
      component: Video
    },
    {
      path: '/users',
      meta: { title: 'Users' },
      beforeEnter (to, from, next) {
        ifAdmin(to, from, next)
        document.title = to.meta.title
        next()
      },
      name: 'users',
      component: Users
    },
    {
      path: '/users/:userId',
      meta: { title: 'User' },
      beforeEnter (to, from, next) {
        document.title = to.meta.title
        next()
      },
      name: 'user',
      component: User
    },
    {
      path: '/users/:userId/playlists',
      meta: { title: 'Playlists' },
      beforeEnter (to, from, next) {
        document.title = to.meta.title
        next()
      },
      name: 'playlists',
      component: Playlists
    },
    {
      path: '/users/:userId/playlists/new',
      meta: { title: 'New Playlist' },
      beforeEnter (to, from, next) {
        document.title = to.meta.title
        next()
      },
      name: 'playlistnew',
      component: NewPlaylist
    },
    {
      path: '/users/:userId/playlists/:id',
      meta: { title: 'Playlist' },
      beforeEnter (to, from, next) {
        document.title = to.meta.title
        next()
      },
      name: 'playlist',
      component: Playlist
    },
    {
      path: '/developer',
      name: 'developer',
      component: () => import('./views/Developer.vue'),
      meta: { title: 'Developer' },
      beforeEnter (to, from, next) {
        document.title = to.meta.title
        next()
      }
    },
    {
      path: '*',
      name: '404',
      component: () => import('./views/404.vue')
    }
  ]
})
